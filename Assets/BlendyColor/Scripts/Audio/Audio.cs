using System;
using UnityEngine;

public class Audio : MonoBehaviour
{
    [SerializeField] private float _sfxVolume = 1f;
    [SerializeField] private float _musicVolume = 1f;

    [SerializeField] private AudioClip[] _sounds;
    [SerializeField] private AudioClip _defaultClip;
    [SerializeField] private AudioClip _gameMusic;

    [field: SerializeField] public AudioSource SourceSFX { get; private set; }
    [field: SerializeField] public AudioSource SourceMusic { get; private set; }
    public static Audio Instance { get; private set; }

       
    public float SfxVolume
    {
        get
        {
            return _sfxVolume;
        }
        set
        {
            _sfxVolume = value;
            SourceSFX.volume = _sfxVolume;

            //SaveSoundVolume(value);
        }
    }


    public float MusicVolume
    {
        get
        {
            return _musicVolume;
        }
        set
        {
            _musicVolume = value;
            SourceMusic.volume = _musicVolume;

            //SaveMusicVolume(value);
        }
    }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }

        else if (Instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        PlayMusic();
    }

    public void PlaySound(string clipName)
    {
        SourceSFX.PlayOneShot(GetSound(clipName), SfxVolume);
    }

    public void PlayMusic()
    {        
        SourceMusic.clip = _gameMusic;
        SourceMusic.volume = MusicVolume;
        SourceMusic.loop = true;
        SourceMusic.Play();
    }

    public void SaveSoundVolume(float soundValue)
    {
        _sfxVolume = soundValue;
    }

    public void SaveMusicVolume(float musicValue)
    {
        _musicVolume = musicValue;
    }

    private AudioClip GetSound(string clipName)
    {
        for (var i = 0; i < _sounds.Length; i++)
            if (_sounds[i].name == clipName) return _sounds[i];

        Debug.LogError("Can not find clip " + clipName);
        return _defaultClip;
    }
}


