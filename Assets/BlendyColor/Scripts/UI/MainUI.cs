using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MainUI : MonoBehaviour
{
    [SerializeField] private LevelManager _levelManager;
    [SerializeField] private ColorManager _colorManager;

    [SerializeField] private PhysicsRaycaster _raycaster;

    [SerializeField] private GameObject _levelVictoryWindow;
    [SerializeField] private GameObject _levelFailWindow;

    [SerializeField] private Slider _musicSlider;
    [SerializeField] private Slider _soundSlider;

    [SerializeField] private GameObject _cloud;
    [SerializeField] private Image _request;
    [SerializeField] private Text _percentValue;

    [SerializeField] private GameObject _mixButton;    

    private Audio _audio;

    public static MainUI Instance { get; private set; }

    public int CompareValue { get; set; }

    public GameObject MixButton { get => _mixButton; set => _mixButton = value; }
    public GameObject Cloud { get => _cloud; set => _cloud = value; }
    

    private void Awake()
    {
        Instance = this;

        _raycaster.enabled = false;        
    }

    private void Start()
    {
        _audio = Audio.Instance;

        UpdateOptions();
    }

    private void OnEnable()
    {
        _levelManager.LevelStarted += OnLevelStarted;
    }

    private void OnDisable()
    {
        _levelManager.LevelStarted -= OnLevelStarted;
    }

    private void OnLevelStarted()
    {
        HideCloud();
        _mixButton.SetActive(false);
    }

    public void PlayClickSound()
    {
        _audio.PlaySound("Click");
    }    

    private void PlayNotepadSheetSound()
    {
        _audio.PlaySound("NotepadSheet");
    }

    public void SetMusicVolume(float volume)
    {
        _audio.MusicVolume = volume;
    }

    public void SetSoundVolume(float volume)
    {
        _audio.SfxVolume = volume;
    }

    public void UpdateOptions()
    {
        _musicSlider.value = _audio.MusicVolume;
        _soundSlider.value = _audio.SfxVolume;
    }

    public void ShowWindow(GameObject window)
    {
        window.SetActive(true);
        window.GetComponentInChildren<Animator>().SetTrigger("Open");

        PlayNotepadSheetSound();
    }

    public void HideWindow(GameObject window)
    {
        window.SetActive(false);
    } 

    public void ShowCloud()
    {
        _cloud.SetActive(true);
        _request.color = _colorManager.ReferenceColor;
        _percentValue.text = "0%";
        _raycaster.enabled = true;
    }

    public void HideCloud()
    {
        _cloud.SetActive(false);
        _raycaster.enabled = false;
    }

    public void ShowVictoryWindow()
    {
        ShowWindow(_levelVictoryWindow);
    }

    public void ShowFailWindow()
    {
        ShowWindow(_levelFailWindow);
    }

    public void CountCompareValue()
    {
        StartCoroutine(Count(CompareValue, 0.02f));
    }

    private void LevelResults()
    {
        if (CompareValue >= 90)
        {
            ShowVictoryWindow();
        }
        else
        {
            ShowFailWindow();
        }
    }

    private IEnumerator Count(int to, float delay)
    {
        _raycaster.enabled = false;

        int count = 0;
        for (int i = 1; i <= to; i++)
        {

            yield return new WaitForSeconds(delay);
            count++;
            
            _percentValue.text = $"{count}%";           
        }

        yield return new WaitForSeconds(2f);

        LevelResults();

        _raycaster.enabled = true;
    }
}
