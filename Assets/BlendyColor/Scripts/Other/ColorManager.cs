using UnityEngine;

public class ColorManager : MonoBehaviour, IStartLevel
{
    [SerializeField] private LevelManager _levelManager;

    private readonly Vector3 D65 = new Vector3(95.047f, 100.000f, 108.883f);
    private readonly string _shaderColorID = "Color_F689E8B9";

    public Color ReferenceColor { get; private set; }


    private void OnEnable()
    {
        _levelManager.LevelStarted += OnLevelStarted;
    }

    private void OnDisable()
    {
        _levelManager.LevelStarted -= OnLevelStarted;
    }
    
    public void OnLevelStarted()
    {        
        ReferenceColor = GetReferenceColor(_levelManager.CurrentLevelData);
    }

    public Color GetReferenceColor(LevelData levelData)
    {
        Color[] ingredientPull = new Color[10];
        GameObject[] ingredientPrefabs = levelData.IngredientPrefab;
        MeshRenderer[] ingredient = new MeshRenderer[ingredientPrefabs.Length];

        int pullValue = Random.Range(4, ingredientPull.Length);

        for (int i = 0; i < ingredientPrefabs.Length; i++)
        {
            Transform child = ingredientPrefabs[i].transform.GetChild(0);
            ingredient[i] = child.gameObject.GetComponent<MeshRenderer>();
        }

        for (int i = 0; i < pullValue; i++)
        {
            int index = Random.Range(0, ingredient.Length);
            ingredientPull[i] = ingredient[index].sharedMaterials[0].GetColor(_shaderColorID);
        }

        Color color = new Color(0, 0, 0, 0);

        foreach (Color col in ingredientPull)
        {
            color += col;
        }

        color /= pullValue;

        return color;
    }

    public int CompareColors(Color firstColor, Color secondColor)
    {
        float distance = GetColorDistance(firstColor, secondColor);

        float delta = 2f;

        int compareValue = Mathf.RoundToInt(100f - distance);

        if (distance <= delta)
        {
            compareValue = 100;
        }
        else if (distance >= 100f)
        {
            compareValue = 0;
        }

        return compareValue;
    }

    private float GetColorDistance(Color firstColor, Color secondColor)
    {
        float[] first = RGBtoLab(firstColor);
        float[] second = RGBtoLab(secondColor);

        float distance = Mathf.Abs(Mathf.Sqrt(Mathf.Pow(first[0] - second[0], 2)
            + Mathf.Pow(first[1] - second[1], 2)
            + Mathf.Pow(first[2] - second[2], 2)));

        return distance;
    }

    private float[] RGBtoLab(Color color)
    {
        //conversion RGB to XYZ
        float[] RGB = { color.r, color.g, color.b };

        for (int i = 0; i < RGB.Length; i++)
        {
            if (RGB[i] > 0.04045)
                RGB[i] = Mathf.Pow((RGB[i] + 0.055f) / 1.055f, 2.4f);
            else
                RGB[i] = RGB[i] / 12.92f;

            RGB[i] *= 100;
        }

        float[] XYZ = {
            RGB[0] * 0.4124f + RGB[1] * 0.3576f + RGB[2] * 0.1805f,
            RGB[0] * 0.2126f + RGB[1] * 0.7152f + RGB[2] * 0.0722f,
            RGB[0] * 0.0193f + RGB[1]* 0.1192f + RGB[2] * 0.9505f };

        //conversion XYZ to LAB
        //Observer= 2�, Illuminant= D65
        XYZ[0] = XYZ[0] / D65.x;
        XYZ[1] = XYZ[1] / D65.y;
        XYZ[2] = XYZ[2] / D65.z;

        for (int i = 0; i < XYZ.Length; i++)
        {
            if (XYZ[i] > 0.008856f) XYZ[i] = Mathf.Pow(XYZ[i], 0.333333f);
            else XYZ[i] = (7.787f * XYZ[i]) + (16 / 116);
        }

        float[] Lab = {
            (116 * XYZ[1]) - 16,
            500 * (XYZ[0] - XYZ[1]),
            200 * (XYZ[1] - XYZ[2]) };

        return Lab;
    }

}