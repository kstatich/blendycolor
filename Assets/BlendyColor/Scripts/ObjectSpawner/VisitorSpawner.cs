using UnityEngine;

public class VisitorSpawner : MonoBehaviour
{
    [SerializeField] private LevelManager _levelManager;

    private LevelData _levelData;
    private GameObject _currentVisitor;

    private void OnEnable()
    {
        _levelManager.LevelStarted += OnLevelStarted;
        _levelManager.LevelCleared += OnLevelCleared;
    }

    private void OnDisable()
    {
        _levelManager.LevelStarted -= OnLevelStarted;
        _levelManager.LevelCleared += OnLevelCleared;
    }

    public void OnLevelStarted()
    {
        _levelData = _levelManager.CurrentLevelData;
        GameObject visitor = _levelData.Visitor;
        SpawnVisitor(visitor);
    }

    public void SpawnVisitor(GameObject visitor)
    {
        _currentVisitor = Instantiate(visitor, visitor.transform.position, visitor.transform.rotation);
    }

    public void OnLevelCleared()
    {
        Destroy(_currentVisitor);
    }
}
