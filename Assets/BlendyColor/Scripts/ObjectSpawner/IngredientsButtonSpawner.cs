using UnityEngine;

public class IngredientsButtonSpawner : MonoBehaviour
{
    [SerializeField] private LevelManager _levelManager;

    private LevelData _levelData;
    private GameObject[] _currentIngredientButtons;

    private Vector3 _startPosition = new Vector3(-0.35f, 1, -1);
    private Vector3 _ingredientsDistance = new Vector3(0.35f, 0, 0);   

    private void OnEnable()
    {
        _levelManager.LevelStarted += OnLevelStarted;
        _levelManager.LevelCleared += OnLevelCleared;
    }

    private void OnDisable()
    {
        _levelManager.LevelStarted -= OnLevelStarted;
        _levelManager.LevelCleared += OnLevelCleared;
    }

    private void OnLevelStarted()
    {
        _levelData = _levelManager.CurrentLevelData;
        GameObject[] ingredients = _levelData.IngredientPrefab;
        SpawnIngredientButtons(ingredients);
    }

    public void SpawnIngredientButtons(GameObject[] ingredients)
    {
        _currentIngredientButtons = new GameObject[ingredients.Length];

        for (int i = 0; i < ingredients.Length; i++)
        {
            GameObject currentIngredientButton = Instantiate(ingredients[i], _startPosition, Quaternion.identity);
           
            _startPosition += _ingredientsDistance;

            _currentIngredientButtons[i] = currentIngredientButton;
        }
    }

    private void OnLevelCleared()
    {
        for (int i = 0; i < _currentIngredientButtons.Length; i++)
        {
            Destroy(_currentIngredientButtons[i]);
        }

        ResetStartPosition();
    }

    private void ResetStartPosition()
    {
        _startPosition = new Vector3(-0.35f, 1, -1);
    }
}


