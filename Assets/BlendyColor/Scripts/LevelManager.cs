using System;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public event Action LevelStarted;
    public event Action LevelCleared;

    private int _currentLevel;

    [field: SerializeField] public List<LevelData> LevelData { get; private set; }
    public LevelData CurrentLevelData { get; private set; }


    private void Start()
    {       
        StartLevel();
    }

    public void StartLevel()
    {
        CurrentLevelData = LevelData[_currentLevel];              
       
        LevelStarted?.Invoke();
    }

    public void ClearLevel()
    {
        LevelCleared?.Invoke();
    }    

    public void NextLevel()
    {
        _currentLevel++;

        if (_currentLevel >= LevelData.Count)
        {
            _currentLevel = 0;
        }

        RestartLevel();
    }

    public void RestartLevel()
    {
        ClearLevel();
        StartLevel();
    }
}
