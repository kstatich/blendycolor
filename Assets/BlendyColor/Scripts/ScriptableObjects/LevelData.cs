using UnityEngine;

[CreateAssetMenu(fileName = "New Level", menuName = "Level Data", order = 51)]
public class LevelData : ScriptableObject
{    
    [field: SerializeField] public GameObject[] IngredientPrefab { get; private set; }
    [field: SerializeField] public GameObject Visitor { get; private set; }
}

