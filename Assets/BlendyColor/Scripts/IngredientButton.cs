using UnityEngine;
using DG.Tweening;
using UnityEngine.EventSystems;

public class IngredientButton : MonoBehaviour, IPointerClickHandler
{
    [SerializeField] private GameObject _ingredientPrefab;

    private Vector3 _ingredientSpawnPoint = new Vector3(0f, 3f, -0.4f);

    private Vector3 _punch = new Vector3(0.4f, 0.4f, 0.4f);
    private float _punchDuration = 0.25f;
    private int _punchVibratio = 6;   
    private float _elasticity = 1f;

    private Audio _audio;

    private void Start()
    {
        _audio = Audio.Instance;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        _audio.PlaySound("FruitPop");

        PunchIngredient();
        SpawnIngredient();        
    }

    private void PunchIngredient()
    {
        gameObject.transform.DOKill(true);
        gameObject.transform.DOPunchScale(_punch, _punchDuration, _punchVibratio, _elasticity);
    }

    private void SpawnIngredient()
    {
        float randomX = Random.Range(0f, 360f);
        float randomY = Random.Range(0f, 360f);
        float randomZ = Random.Range(0f, 360f);

        Quaternion ingredientRotation = new Quaternion(randomX, randomY, randomZ, 0);

        GameObject ingredient = Instantiate(_ingredientPrefab, _ingredientSpawnPoint, ingredientRotation);
        ingredient.gameObject.AddComponent<MeshCollider>().convex = true;
        ingredient.gameObject.AddComponent<Rigidbody>().collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;        
    }
}
