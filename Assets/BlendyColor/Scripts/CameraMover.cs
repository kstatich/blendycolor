using UnityEngine;
using DG.Tweening;

public class CameraMover : MonoBehaviour
{
    [SerializeField] private LevelManager _levelManager;

    private void OnEnable()
    {
        _levelManager.LevelStarted += OnLevelStarted;
    }

    private void OnDisable()
    {
        _levelManager.LevelStarted -= OnLevelStarted;
    }

    private void OnLevelStarted()
    {
        MoveCamera();
    }

    private void MoveCamera()
    {
        transform.position = new Vector3(0f, 1.7f, -1f);

        MainUI mainUI = MainUI.Instance;
        transform.DOMove(new Vector3(0f, 2.05f, -2.2f), 2f).OnComplete(mainUI.ShowCloud);
    }
}
